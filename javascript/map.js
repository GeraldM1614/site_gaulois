function initMap() {
    var gaulois = {lat: 45.547823, lng: -73.651653};
    var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 13,
    center: gaulois
      });
    var marker = new google.maps.Marker({
      position: gaulois,
      map: map
      });
  }
